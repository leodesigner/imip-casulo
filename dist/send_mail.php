<?php

$errorMSG = "";

// NAME
if (empty($_POST["name2"])) {
    $errorMSG = "Insira um nome ";
} else {
    $name2 = $_POST["name2"];
}

// EMAIL
if (empty($_POST["email2"])) {
    $errorMSG .= "Insira um E-mail";
} else {
    $email2 = $_POST["email2"];
}

// TELEFONNE
if (empty($_POST["telefone2"])) {
    $errorMSG .= "Insira um Telefone";
} else {
    $telefone2 = $_POST["telefone2"];
}

// Cidade
if (empty($_POST["cidade2"])) {
    $errorMSG .= "Selecione uma Cidade";
} else {
    $cidade2 = $_POST["cidade2"];
}

// UF
if (empty($_POST["uf2"])) {
    $errorMSG .= "Selecione uma Estado";
} else {
    $uf2 = $_POST["uf2"];
}

// Assunto
if (empty($_POST["assunto"])) {
    $errorMSG .= "Insira um assunto";
} else {
    $assunto = $_POST["assunto"];
}


// MESSAGE
if (empty($_POST["message"])) {
    $errorMSG .= "A caixa de mensagem é origatória";
} else {
    $message = $_POST["message"];
}


$EmailTo = "secretariaacademica@fps.edu.br";
$Subject = "Formulário de Congresso IMIP - FBV";

// prepare email body text
$Body = "";
$Body .= "Nome: ";
$Body .= $nome2;
$Body .= "\n";
$Body .= "Telefone: ";
$Body .= $telefone2;
$Body .= "\n";
$Body .= "Email: ";
$Body .= $email2;
$Body .= "\n";
$Body .= "Cidade: ";
$Body .= $cidade2;
$Body .= "\n";
$Body .= "UF: ";
$Body .= $uf2;
$Body .= "\n";
$Body .= "Assunto: ";
$Body .= $assunto;
$Body .= "\n";
$Body .= "Messagem: ";
$Body .= $message;
$Body .= "\n";

// send email
$success = mail($EmailTo, $Subject, $Body, "From:".$email);

// redirect to success page
if ($success && $errorMSG == ""){
   echo "success";
}else{
    if($errorMSG == ""){
        echo "Something went wrong :(";
    } else {
        echo $errorMSG;
    }
}

?>