$("#cadastroForm").validator().on("submit", function (event) {
    if (event.isDefaultPrevented()) {
        // handle the invalid form...
        formError();
        submitMSG(false, "Você precisa preencher o formulário corretamente");
    } else {
        // everything looks good!
        event.preventDefault();
        submitForm();
    }
});


function submitForm(){
    // Initiate Variables With Form Content
    var nome2 = $("#nome2").val();
    var email2 = $("#email2").val();
    var cidade2 = $("#cidade2").val();
    var uf2 = $("#uf2").val();
    var telefone2 = $("#telefone2").val();
    var assunto = $("#assunto").val();
    var message = $("#message").val();


    $.ajax({
        type: "POST",
        url: "send_mail.php",
        data: "nome2=" + nome2 + "&email2=" + email2 + "&cidade2=" + cidade2 + "&uf2=" + uf2 + "&telefone2=" + telefone2 + "&assunto=" + assunto + "&message=" + message,
        success : function(text){
            if (text == "success"){
                formSuccess();
            } else {
                formError();
                submitMSG(false,text);
            }
        }
    });
}

function formSuccess(){
    $("#cadastroForm")[0].reset();
    submitMSG(true, "Mensagem enviada com sucesso!")
}

function formError(){
    $("#cadastroForm").removeClass().addClass('shake animated').one('webkitAnimationEnd mozAnimationEnd MSAnimationEnd oanimationend animationend', function(){
        $(this).removeClass();
    });
}

function submitMSG(valid, msg){
    if(valid){
        var msgClasses = "h3 text-center tada animated text-success";
    } else {
        var msgClasses = "h3 text-center text-danger";
    }
    $("#msgSubmit").removeClass().addClass(msgClasses).text(msg);
}